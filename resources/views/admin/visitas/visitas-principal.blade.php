@extends('admin.base-admin')

@section('content')
<br>

	<div class="container">
		<div class="row">
			<div class="col-md-8">
				{{-- <button id="nueva-receta">Nueva Receta</button> --}}
			</div>
			<div class="col-md-4 text-right">
				<a href="{{url('admin')}}">Volver</a>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-12">
				<table class="table table-bordered table-condensed">
					<thead>
						<tr>
							<th class="text-center">Nº</th>
							<th class="text-center">IP</th>
							<th class="text-center">Country</th>
							<th class="text-center">Clicks</th>
							<th class="text-center">Registrada</th>
							
						</tr>
					</thead>
					<tbody>
						<?php $i=0; ?>
						@foreach($visitas as $visita)
						<tr>
							<td class="text-center">{{$i=$i+1}}</td>
							<td class="text-center">{{$visita->ip}}</td>
							<td class="text-center">{{$visita->country}}</td>
							<td class="text-center">{{$visita->clicks}}</td>
							<td class="text-center">{{$visita->created_at}}</td>
							
							{{-- <td class="text-center">
								
								<button onclick="editReceta({{$receta->id}})"><span class="glyphicon glyphicon-edit" ></span></button>
								<button><span class="glyphicon glyphicon-trash"></span></button>
								<button><span class="glyphicon glyphicon-new-window"></span></button>
							</td> --}}
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			
		</div>
		
	</div>
	
@endsection

@section('scripts')
	
@endsection