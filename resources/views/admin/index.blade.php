@extends('admin.base-admin')

@section('content')
	<br>
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="panel panel-default">
				  <!-- Default panel contents -->
				  <div class="panel-heading"><b>CONTENIDO DE RECETAS</b></div>
				  {{-- <div class="panel-body">
				    <p><small>Contenido en la web.</small></p>
				  </div> --}}

				  <!-- List group -->
				  <ul class="list-group">
				    <li class="list-group-item">
				    	<div class="row">
				    		<div class="col-md-8">
				    			<b><a href="{{url('admin/recetas/lista')}}">Recetas</a></b>
				    		</div>
				    		<div class="col-md-4 text-right">
				    			<button id="add-receta" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-plus"></span></button>
				    		</div>
				    	</div>
				    	
				    </li>
				    <li class="list-group-item">
				    	<div class="row">
				    		<div class="col-md-8">
				    			<b><a href="{{url('admin/categorias/lista')}}">Categorías</a></b>
				    		</div>
				    		<div class="col-md-4 text-right">
				    			<button id="add-receta" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-plus"></span></button>
				    		</div>
				    	</div>
				    	
				    </li>
				    <li class="list-group-item">
				    	<div class="row">
				    		<div class="col-md-8">
				    			<b><a href="{{url('admin/imagenes/lista')}}">Imàgenes</a></b>
				    		</div>
				    		<div class="col-md-4 text-right">
				    			<a id="add-receta" href="{{url('admin/imagen/crear')}}" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-plus"></span></a>
				    		</div>
				    	</div>
				    	
				    </li>
				    
				  </ul>
				</div>

				<!-- otro grupo de items -->

				<div class="panel panel-default">
				  <!-- Default panel contents -->
				  <div class="panel-heading"><b>BLOG</b></div>
				  {{-- <div class="panel-body">
				    <p><small>Contenido en la web.</small></p>
				  </div> --}}

				  <!-- List group -->
				  <ul class="list-group">
				    <li class="list-group-item">
				    	<div class="row">
				    		<div class="col-md-8">
				    			<b><a href="{{url('admin/recetas/lista')}}">Entradas</a></b>
				    		</div>
				    		<div class="col-md-4 text-right">
				    			<button id="add-receta" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-plus"></span></button>
				    		</div>
				    	</div>
				    	
				    </li>
				    
				  </ul>
				</div>

				<div class="panel panel-default">
				  <!-- Default panel contents -->
				  <div class="panel-heading"><b>VISITAS</b></div>
				  {{-- <div class="panel-body">
				    <p><small>Contenido en la web.</small></p>
				  </div> --}}

				  <!-- List group -->
				  <ul class="list-group">
				    <li class="list-group-item">
				    	<div class="row">
				    		<div class="col-md-8">
				    			<b><a href="{{url('admin/visitas/principal')}}">Visitas a Pàgina Principal</a></b>
				    		</div>
				    		<div class="col-md-4 text-right">
				    			{{-- <button id="add-receta" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-plus"></span></button> --}}
				    		</div>
				    	</div>
				    	
				    </li>
				    
				  </ul>
				</div>
			</div>
		</div>
	</div>
	
@endsection

