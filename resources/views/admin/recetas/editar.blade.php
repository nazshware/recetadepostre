@extends('admin.base-admin')

@section('style')
	<link rel="stylesheet" href="{{asset('css/summernote.css')}}">
@endsection

@section('content')

<div class="container">
    <br>
	<div class="row">
		<div class="col-md-8">
			
		</div>
		<div class="col-md-4 text-right">
			<a href="{{url('admin/recetas/lista')}}">Volver</a>
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-md-8">
			<h1>Editar Receta</h1>

			{!! Form::open(['url' => 'admin/receta/editar/guardar/'.$receta->id, 'method'=>'PUT' ]) !!}
				<div class="form-group">
					{{ Form::label('Titulo de Receta') }}
				    {{ Form::text('titulo', $receta->titulo_receta, ['class' => 'form-control']) }}
			    </div>
			    <div class="form-group">
					{{ Form::label('Breve Descripcion') }}
				    {{ Form::textarea('descripcion', $receta->breve_descripcion, ['class' => 'form-control', 'rows'=>'3']) }}
			    </div>

			    <div class="form-group">
					{{ Form::label('Descripcion Imagen') }}
				    {{ Form::text('descripcion_imagen', $receta->imagen_descripcion, ['class' => 'form-control']) }}
			    </div>
			    <div class="form-group">
					{{ Form::label('Contenido Intro') }}
				    {{ Form::textarea('contenido_intro', $receta->contenido_receta_intro, ['class' => 'form-control', 'rows'=>'3', 'id'=>'contenido_intro' ]) }}
			    </div>
			    <div class="form-group">
					{{ Form::label('Contenido') }}
				    {{ Form::textarea('contenido', $receta->contenido_receta, ['class' => 'form-control', 'rows'=>'6', 'id'=>'contenido' ]) }}
			    </div>
			    <div class="form-group">
					{{ Form::label('Contenido Final') }}
				    {{ Form::textarea('contenido_final', $receta->contenido_receta_final, ['class' => 'form-control', 'rows'=>'3', 'id'=>'contenido_final' ]) }}
			    </div>
			    <div class="form-group">
					{{ Form::label('Categoria') }}
				    <select name="categoria" id="" class="form-control">
				    	@foreach($categorias as $categoria)
				    		@if($categoria->id==$receta->categoria_id)
								<option value="{{$categoria->id}}" selected>{{$categoria->titulo_categoria}}</option>
							@else
								<option value="{{$categoria->id}}">{{$categoria->titulo_categoria}}</option>
				    		@endif
							
				    	@endforeach
				    </select>
			    </div>
			    <div class="form-group">
				    {{ Form::submit('Guardar Cambios') }}
			    </div>
			{!! Form::close() !!}
		</div>
	</div>	
	
</div>


@endsection

@section('scripts')
	<script src="{{asset('js/summernote.js')}}"></script>
	<script>
		$('#contenido').summernote({
            height:300,
        });
        $('#contenido_intro').summernote({
            height:150,
        });
        $('#contenido_final').summernote({
            height:150,
        });
	</script>
@endsection