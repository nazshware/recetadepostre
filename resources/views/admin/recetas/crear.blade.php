@extends('admin.base-admin')

@section('style')
	<link rel="stylesheet" href="{{asset('css/summernote.css')}}">
@endsection

@section('content')

<div class="container">
    <br>
	<div class="row">
		<div class="col-md-8">
			
		</div>
		<div class="col-md-4 text-right">
			<a href="{{url('admin')}}">Volver</a>
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-md-8">
			<h1>Crear Receta</h1>

			{!! Form::open(['url' => 'admin/receta/guardar', 'files'=>'true']) !!}
				<div class="form-group">
					{{ Form::label('Categoría') }}
				    <select name="categoria" id="" class="form-control">
				    	@foreach($categorias as $categoria)
						<option value="{{$categoria->id}}">{{$categoria->titulo_categoria}}</option>
				    	@endforeach
				    </select>
			    </div>
				<div class="form-group">
					{{ Form::label('Título de Receta') }}
				    {{ Form::text('titulo', null, ['class' => 'form-control']) }}
			    </div>
			    <div class="form-group">
					{{ Form::label('Breve Descripción') }}
				    {{ Form::textarea('descripcion', null, ['class' => 'form-control', 'rows'=>'3']) }}
			    </div>
			    <div class="form-group">
					{{ Form::label('Imagen Principal') }}
				    {{ Form::file('imagen', null, ['class' => 'form-control']) }}
			    </div>
			    <div class="form-group">
					{{ Form::label('Descripción Imagen') }}
				    {{ Form::text('descripcion_imagen', null, ['class' => 'form-control']) }}
			    </div>
			    <div class="form-group">
					{{ Form::label('Contenido Intro') }}
				    {{ Form::textarea('contenido_intro', null, ['class' => 'form-control', 'rows'=>'6', 'id'=>'contenido_intro' ]) }}
			    </div>
			    {{-- <div class="form-group">
			    	{{ Form::label('Imagenes Adicionales 1') }}
				    <select name="" id="" class="form-control" multiple>
				    	@foreach($imagenes as $imagen)
							<option value="{{ $imagen->id }}">{{ $imagen->imagen }}</option>
						@endforeach
					</select>
				</div> --}}
			    <div class="form-group">
					{{ Form::label('Contenido Receta') }}
				    {{ Form::textarea('contenido', null, ['class' => 'form-control', 'rows'=>'6', 'id'=>'contenido' ]) }}
			    </div>
			    {{-- <div class="form-group">
			    	{{ Form::label('Imagenes Adicionales 2') }}
				    <select name="" id="" class="form-control" multiple>
				    	@foreach($imagenes as $imagen)
							<option value="{{ $imagen->id }}">{{ $imagen->imagen }}</option>
						@endforeach
					</select>
				</div> --}}
			    <div class="form-group">
					{{ Form::label('Contenido Final') }}
				    {{ Form::textarea('contenido_final', null, ['class' => 'form-control', 'rows'=>'6', 'id'=>'contenido_final' ]) }}
			    </div>
			    
			    <div class="form-group text-right">
				    {{ Form::submit('Guardar', ['class' => 'btn btn-primary']) }}
			    </div>
			{!! Form::close() !!}
		</div>
	</div>	
	
</div>


@endsection

@section('scripts')
	<script src="{{asset('js/summernote.js')}}"></script>
	<script>
		$('#contenido').summernote({
            height:300,
        });
        $('#contenido_final').summernote({
            height:150,
        });
        $('#contenido_intro').summernote({
            height:150,
        });
	</script>
@endsection