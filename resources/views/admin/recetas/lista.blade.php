@extends('admin.base-admin')

@section('content')
<br>

	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<button id="nueva-receta">Nueva Receta</button>
			</div>
			<div class="col-md-4 text-right">
				<a href="{{url('admin')}}">Volver</a>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-12">
				<table class="table table-bordered table-condensed">
					<thead>
						<tr>
							<th class="text-center">Nº</th>
							<th class="text-center">Recetas</th>
							<th class="text-center">Creada</th>
							<th class="text-center">Modificada</th>
							<th class="text-center">Opciones</th>
						</tr>
					</thead>
					<tbody>
						<?php $i=0; ?>
						@foreach($recetas as $receta)
						<tr>
							<td class="text-center">{{$i=$i+1}}</td>
							<td><a href="{{url('admin/receta/editar')}}/{{$receta->id}}">{{$receta->titulo_receta}}</a></td>
							<td class="text-center">{{$receta->created_at}}</td>
							<td class="text-center">{{$receta->updated_at}}</td>
							<td class="text-center">
								
								<button onclick="editReceta({{$receta->id}})"><span class="glyphicon glyphicon-edit" ></span></button>
								<button><span class="glyphicon glyphicon-trash"></span></button>
								<button><span class="glyphicon glyphicon-new-window"></span></button>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			
		</div>
		
	</div>
	
@endsection

@section('scripts')
	<script>
		$('#nueva-receta').click(function(){
			location.href = '{{url('admin/receta/crear')}}';
		});

		function editReceta(id){
			location.href = '{{url('admin/receta/editar')}}/'+id;
		}	
	</script>
@endsection