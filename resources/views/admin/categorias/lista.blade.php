@extends('admin.base-admin')

@section('content')
<br>

	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<button id="nueva-categoria" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-plus"></span> Nueva Categoria</button>
			</div>
			<div class="col-md-4 text-right">
				<a href="{{url('admin')}}">Volver Admin</a>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-12">
				<table class="table table-bordered table-condensed">
					<thead>
						<tr>
							<th class="text-center">Nº</th>
							<th class="text-center">Categorias</th>
							<th class="text-center">Creada</th>
							<th class="text-center">Modificada</th>
							<th class="text-center">Opciones</th>
						</tr>
					</thead>
					<tbody>
						<?php $i=0; ?>
						@foreach($categorias as $categoria)
						<tr>
							<td class="text-center">{{$i=$i+1}}</td>
							<td><a href="{{url('admin/categoria/editar')}}/{{$categoria->id}}">{{$categoria->titulo_categoria}}</a></td>
							<td class="text-center">{{$categoria->created_at}}</td>
							<td class="text-center">{{$categoria->updated_at}}</td>
							<td class="text-center">
								
								<button onclick="editCategoria({{$categoria->id}})"><span class="glyphicon glyphicon-edit" ></span></button>
								<button><span class="glyphicon glyphicon-trash"></span></button>
								<button><span class="glyphicon glyphicon-new-window"></span></button>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			
		</div>
		
	</div>
	
@endsection

@section('scripts')
	<script>
		$('#nueva-categoria').click(function(){
			location.href = '{{url('admin/categoria/crear')}}';
		});

		function editCategoria(id){
			location.href = '{{url('admin/categoria/editar')}}/'+id;
		}	
	</script>
@endsection