@extends('admin.base-admin')

@section('content')

<div class="container">
    <br>
	<div class="row">
		<div class="col-md-8">
			
		</div>
		<div class="col-md-4 text-right">
			<a href="{{url('admin')}}">Volver</a>
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-md-8">
			<h1>Crear Categoria</h1>

			{!! Form::open(['url' => 'admin/categoria/guardar']) !!}
				<div class="form-group">
					{{ Form::label('Titulo de Categoria') }}
				    {{ Form::text('titulo', null, ['class' => 'form-control']) }}
			    </div>
			    <div class="form-group">
				    {{ Form::submit('Guardar') }}
			    </div>
			{!! Form::close() !!}
		</div>
	</div>	
	
</div>


@endsection

@section('scripts')
	
@endsection