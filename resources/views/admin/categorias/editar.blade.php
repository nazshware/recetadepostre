@extends('admin.base-admin')



@section('content')

<div class="container">
    <br>
	<div class="row">
		<div class="col-md-8">
			
		</div>
		<div class="col-md-4 text-right">
			<a href="{{url('admin/categorias/lista')}}">Volver</a>
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-md-8">
			<h1>Editar Categoria</h1>

			{!! Form::open(['url' => 'admin/receta/editar/guardar/'.$categoria->id, 'method'=>'PUT' ]) !!}
				<div class="form-group">
					{{ Form::label('Titulo de Categoria') }}
				    {{ Form::text('titulo', $categoria->titulo_categoria, ['class' => 'form-control']) }}
			    </div>
			    
			    <div class="form-group">
				    {{ Form::submit('Guardar Cambios') }}
			    </div>
			{!! Form::close() !!}
		</div>
	</div>	
	
</div>


@endsection

@section('scripts')
	
@endsection