@extends('admin.base-admin')

@section('content')

<div class="container">
    <br>
	<div class="row">
		<div class="col-md-8">
			
		</div>
		<div class="col-md-4 text-right">
			<a href="{{url('admin')}}">Volver</a>
		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-md-8">
			<h1>Nueva Imagen</h1>

			{!! Form::open(['url' => 'admin/imagen/guardar', 'files'=>'true']) !!}
				<div class="form-group">
					{{ Form::label('Recetas') }}
				    <select name="receta_id" id="" class="form-control">
				    	@foreach($recetas as $receta)
				    	<option value="{{$receta->id}}">{{$receta->titulo_receta}}</option>
				    	@endforeach
				    </select>
			    </div>
				<div class="form-group">
					{{ Form::label('Titulo de Imagen') }}
				    {{ Form::text('titulo_imagen', null, ['class' => 'form-control']) }}
			    </div>
			    <div class="form-group">
					{{ Form::label('Descripcion') }}
				    {{ Form::textarea('descripcion_imagen', null, ['class' => 'form-control', 'rows'=>'3']) }}
			    </div>
			    <div class="form-group">
					{{ Form::label('Imagen') }}
				    {{ Form::file('imagen_receta', null, ['class' => 'form-control']) }}
			    </div>
			    <div class="form-group text-right">
				    {{ Form::submit('Guardar', ['class' => 'btn btn-success']) }}
			    </div>
			{!! Form::close() !!}
		</div>
	</div>	
	
</div>

@endsection