@extends('admin.base-admin')

@section('content')

	<div class="container">
		<div class="row">
	
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<b>{{ $imagen->imagen }}</b>
					</div>
					<div class="panel-body">
						
						<figure>
							<img src="{{ asset('uploads/imagenes/recetas') }}/{{ $imagen->imagen }}" alt="" class="img-responsive img-block">
						</figure>
							
						
						
					</div>
					
				</div>
				
			</div>

		</div>
	</div>
	

@endsection