@extends('admin.base-admin')

@section('content')
<br>

	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<button id="nueva-imagen" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-plus"></span> Nueva Imagen</button>
			</div>
			<div class="col-md-4 text-right">
				<a href="{{url('admin')}}">Volver Admin</a>
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-12">
				<table class="table table-bordered table-condensed">
					<thead>
						<tr>
							<th class="text-center">Nº</th>
							<th class="text-center">Tìtulo Imagen</th>
							<th class="text-center">Link Imagen</th>
							<th class="text-center">Receta que pertenece</th>
							<th class="text-center">Creada</th>
							<th class="text-center">Opciones</th>
						</tr>
					</thead>
					<tbody>
						<?php $i=0; ?>
						@foreach($imagenes as $imagen)	
						<tr>
							<td class="text-center">{{$i=$i+1}}</td>
							<td>{{$imagen->titulo_imagen}}</td>
							<td class="text-center"><a href="{{url('admin/uploads/imagenes')}}/{{$imagen->id}}">{{$imagen->imagen}}</a></td>
							<td class="text-center">
								@foreach($recetas as $receta)
									@if($receta->id==$imagen->receta_id)
										{{$receta->titulo_receta}}
									@endif
								@endforeach
							</td>
							<td class="text-center">{{$imagen->created_at}}</td>
							<td class="text-center">
								<button  class="btn btn-default btn-xs"><span class="glyphicon glyphicon-edit" ></span></button>
								<button class="btn btn-default btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
								<button class="btn btn-default btn-xs"><span class="glyphicon glyphicon-new-window"></span></button>
							</td>
						</tr>
						@endforeach
					</tbody>
					
				</table>
			</div>
			
		</div>
		
	</div>
@endsection

@section('scripts')
	<script>
		$('#nueva-imagen').click(function(){
			location.href = '{{url('admin/imagen/crear')}}';
		});

		// function editCategoria(id){
		// 	location.href = '{{url('admin/categoria/editar')}}/'+id;
		// }	
	</script>
@endsection