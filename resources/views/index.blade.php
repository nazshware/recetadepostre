@extends('base')

@section('metas')
<meta name="description" content="Recetas de postres de tu preferencia y conocimiento adicional para que puedas preparar un buen postre en casa. Nuestro saber es tu sabor.">
@endsection

@section('content')

	<header>
    <div class="header-wrap">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 class="title-portada text-center">Receta d'postre</h1>
            <h3 class="text-center color-black">
              Nuestro saber es tu Sabor
            </h3>
            
          </div>
        </div>
      </div>
    </div>
    
  </header>
	
	<main>
		<div class="section-all">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2>_Nuestras recetas recientes</h2>
						<br>
					</div>
					

					@foreach($recetas_array as $receta)
					
					
					<div class="col-md-4">
						<div class="panel panel-default">
						  <div class="panel-body">
						  	
						  	<a href="/{{$receta['categoria_slug']}}/{{$receta[0]['slug']}}">
						    	<img src="{{asset("uploads/imagenes/recetas")}}/{{$receta[0]['imagen']}}" alt="" class="img-responsive">
						    </a>
						  </div>
						  <div class="panel-footer">
						  	<h3>{{$receta[0]['titulo_receta']}}</h3>
						  	<p>{{$receta[0]['breve_descripcion']}}</p>
						  </div>
						</div>
					</div>
					
					@endforeach

					
				</div>
			</div>
		</div>
		
		<div class="section-wrap">
			<div class="section-phrase">
				<section class="container">
					<div class="row">
						<div class="col-md-12">
							<h2 class="text-center"><span style="font-size: 1.5em; font-weight: bold">_S</span>omos personas que intentamos brindarte lo mejor de nuestros conocimientos en postres para que puedas preparar fácil y rápido lo mejor en casa.</h2>

						</div>
					</div>
					<br>
					<div class="row">						
						<div class="col-md-2 col-md-offset-5 col-xs-12">
							<a class="btn btn-warning btn-lg" href="{{ url('recetas') }}">Ir a Recetas</a>
						</div>
					</div>
				</section>
			</div>
		</div>

		
		
	</main>
	
@endsection

@section('scripts')
	<script>
		$('.img-link').mouseover(function(){
			$(this).children().slideDown(400);
		});
		$('.img-link').mouseleave(function(){
			$(this).children().slideUp(300);
		});
	</script>
@endsection