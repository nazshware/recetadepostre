<div class="section-all">
			<div class="container">
				<div class="row">

					<div class="col-md-8 col-md-offset-2 text-center">
						<h2>_Consejos</h2>
						
						<p style="font-size: 1.4em">
							Se recomienda siempre colocarse un gorro repostero o algo que nos cubra los cabellos para prevenir que estos puedan caer en nuestras preparaciones de postres.
						</p>
						<div>
							<span class="glyphicon glyphicon-heart" style="font-size: 3em"></span>
						</div>
					</div>

					
				</div>
			</div>
		</div>

<footer>
	<div class="footer-inside">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<h4 class="color-black subtitle-footer">_SOMOS</h4>
					<div class="row" >
						<div class="col-md-5">
							<img src="{{ asset('/img/logo.png') }}" alt="" class="img-responsive"> 
						</div>
						<div class="col-md-7">
							<p class="color-black" style="font-size: 1.2em">
								Somos personas que intentamos brindarte lo mejor de nuestros conocimientos en postres para que puedas preparar facil y rapido lo mejor en casa.
							</p>
						</div>
						
					</div>
					
					
				</div>
				<div class="col-md-4">
					<h4 class=" color-black subtitle-footer">_NUESTRAS REDES</h4>
					<p >
						<div class="color-black" >
							<a href="https://www.facebook.com/Receta-de-Postre-1310396852397684/" target="_blank" style="text-decoration:none; color:black; font-size: 1.3em"><img src="{{ asset('/img/fb-logo.png') }}" width="24px"> @recetadpostre</a>
						</div>

						<div class="color-black" style="margin-top: .5em">
							<a href="#" target="_blank" style="text-decoration:none; color:black; font-size: 1.3em"><img src="{{ asset('/img/instagram.png') }}" width="24px"> /recetadpostre</a>
						</div>
					</p>
				</div>
				<div class="col-md-4">
					<h4 class="color-black subtitle-footer">_AUSPICIADORES</h4>
					<p class="color-black" >
						
						
						<div class="color-black" style="font-size: 1.2em">
							Tortas y Postres "Donde Charito"
						</div>

						<div class="color-black" style="margin-top: .5em; font-size: 1.2em">
							Negocios y Servicios Multiples "Charito"
						</div>
					</p>
						
						
					</p>
				</div>
			</div>
		</div>
	</div>
	
</footer>
<div class="sub-footer text-center">
	Desarrollado por @jhonazsh
	
</div>