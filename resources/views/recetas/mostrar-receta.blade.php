@extends('base')

@section('content')

	
	<main>
		<div class="container section-all">
			<div class="col-md-8 col-md-offset-2">
				<h1 class="text-center">
					_{{$receta_encontrada['titulo_receta']}}
				</h1>
				<br>
				<h4>
					{{$receta_encontrada['breve_descripcion']}} 
				</h4>	
				<br>
				<figure>
					<a href="{{url('imagenes/principales')}}/{{$receta_encontrada['imagen']}}"><img src="{{asset('uploads/imagenes/recetas')}}/{{$receta_encontrada['imagen']}}" class="img-responsive" alt="{{$receta_encontrada['imagen_descripcion']}}" ></a>
				</figure>
				<div class="extras">
					
					<div class="row">
						<div class="col-md-7">
							<b>Tiempo aproximado de Preparación: </b> <span class="color-black">{{$extras['tiempo_preparacion']}}</span>
						</div>
						<div class="col-md-5">
							<b>Cantidad Porciones: </b> <span class="color-black">{{$extras['porciones']}}</span>
						</div>
					</div>	
							
							
						
					
				</div>
				
				<div class="contenido-word">
					{!!$receta_encontrada['contenido_receta_intro']!!}
				</div>

				<div class="recetario-referencia">	
					Recetas que te pueden interesar:	
				</div>
				
				<div class="contenido-word">
					
					{!!$receta_encontrada['contenido_receta']!!}
					
				</div>

				<div class="row" style="margin-top: 3em; margin-bottom: 1em">
				@foreach($imagenes as $imagen)
						
						<div class="col-md-6" style="margin-bottom: 2em">
							<figure>	
								<img src="{{asset('uploads/imagenes/recetas')}}/{{$imagen->imagen}}" alt="" class="img-responsive">
							</figure>
							<h4>{{$imagen->titulo_imagen}}</h4>	
						</div>

				@endforeach
				</div>

				<div class="contenido-word">
					{!!$receta_encontrada['contenido_receta_final']!!}
				</div>
				
				<div class="fb-comments" data-href="http://recetadepostre.com" data-width="100%" data-numposts="10"></div>
			</div>
			
		</div>
	</main>


<div class="modal fade" tabindex="-1" role="dialog" id="modalI">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->	
	
@endsection

@section('scripts')
	<script>
		function showBigImage(){
			$('#modalI').modal('show');
		}
	</script>
@endsection