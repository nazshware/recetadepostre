@extends('../base')

@section('content')

<main id="app-search">
	<div class="container">
		<div class="section-all">
			<div class="row">
				<div class="col-md-6">
					<h2>_Todas nuestras recetas</h2>
				</div>
				<div class="col-md-6" style="margin-top: 1em">
					<div class="input-group">
					  <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-search"></span></span>
					  <input type="text" class="form-control" placeholder="Buscar Receta..." aria-describedby="basic-addon1" v-model="wordSearch" v-on:keyup="onKeySearch">
					</div>
				</div>
			</div>
		</div>


	</div>

    <div class="container">
		<div class="section-all">

			<!-- muestra los datos desde el controlador de laravel -->
			<div class="row" v-if="searchActivate===false" v-bind:style="showContent">
			
				
				@foreach($recetas_array as $receta)
					
					
					<div class="col-md-4">
						<div class="panel panel-default">
						  <div class="panel-body">
						  	
						  	<a href="/{{$receta['categoria_slug']}}/{{$receta[0]['slug']}}">
						    	<img src="{{asset("uploads/imagenes/recetas")}}/{{$receta[0]['imagen']}}" alt="" class="img-responsive">
						    </a>
						  </div>
						  <div class="panel-footer">
						  	<h3>{{$receta[0]['titulo_receta']}}</h3>
						  	<p>{{$receta[0]['breve_descripcion']}}</p>
						  </div>
						</div>
					</div>
					
				@endforeach

				
			</div>

			<!-- muestra los datos una vez que se activa la busqueda -->
			<div class="row" v-else v-bind:style="showContentSearch">
				<div v-show="contValuesData!=0">
					<div class="col-md-4" v-for="item in valuesData">
						<div class="panel panel-default">
						  <div class="panel-body">
						  	
						  	<a v-bind:href="'/'+item['categoria_slug']+'/'+item[0].slug+''">
						    	<img :src="'{{asset("uploads/imagenes/recetas")}}/'+item[0].imagen" class="img-responsive">
						    </a>
						  </div>
						  <div class="panel-footer">

						  	<h3>@{{item[0].titulo_receta}}</h3>
						  	<p>@{{item[0].breve_descripcion}}</p>
						  </div>
						</div>
					</div>
				</div>
				<div v-show="contValuesData===0" class="text-center">
					<h3 style="color:#b5b5b5">No se encontraron resultados con la búsqueda...</h3>
				</div>
				
			</div>
		</div>
	</div>
</main>			

@endsection

@section('scripts')
<script>
	const appSearch = new Vue({
		el: '#app-search',
		data:{
			searchActivate:false,
			showContent:"display:block",
			showContentSearch:"display:none",
			wordSearch:"",
			valuesData: [],
			contValuesData : ""
		},
		methods:{
			onKeySearch: function(){
				if(this.wordSearch!=""){
					this.searchActivate = true;
					this.showContent = "display:none";
					this.showContentSearch = "display:block";
					
					$.ajax({
						url:'{{ url("ajax/get/search/receta") }}',
						data:{
							word: this.wordSearch
						},
						type:'get',
						success: function(data){
							appSearch.valuesData = data;
							appSearch.contValuesData = data.length;
							
						}
					})
				}
				else{
					this.searchActivate = false;
					this.showContent = "display:block";
					this.showContentSearch = "display:none";
				}
			}
		}
	});
</script>
 

@endsection