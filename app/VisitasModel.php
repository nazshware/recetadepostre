<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitasModel extends Model
{
    protected $table = "visitor_registry";

    protected $fillable = [
    	'ip',
    	'country',
    	'clicks'
    ];
}
