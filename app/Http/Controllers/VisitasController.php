<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\VisitasModel;

use GeoIP;

class VisitasController extends Controller
{
    public function principal(){
    	$visitas = VisitasModel::all();

    	// $visitas_array = [];
    	// $i=0;

    	// foreach ($visitas as $visita) {
    	// 	$visitas_array[$i] = [
    	// 		'ip'=>$visita->ip,
    	// 		'country'=>'',
    	// 		'clicks'=>$visita->clicks,
    	// 		'created_at'=>$visita->created_at
    	// 	];
    	// 	$i=$i+1;
    	// }

    	// $location = geoip($visita->ip);
    	// return $location;

    	return view('admin.visitas.visitas-principal', compact('visitas'));
    }	
}
