<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\RecetaModel;
use \App\ImagenesRecetaModel;
use \App\ExtrasRecetaModel;

use Visitor;

class RecetaPublicController extends Controller
{
    public function mostrarReceta($categoria, $slug, Request $request){
    	$receta_encontrada = RecetaModel::where('slug','=',$slug)->first();
    	$imagenes = ImagenesRecetaModel::where('receta_id','=',$receta_encontrada['id'])->get();
    	$extras = ExtrasRecetaModel::where('receta_id','=',$receta_encontrada['id'])->first();


    	return view('recetas.mostrar-receta', compact('receta_encontrada','imagenes','extras'));

    	//return Visitor::all();
    	
    }	

    public function getAllRecetas(){
    	$recetas = \App\RecetaModel::all();
    	$categorias = \App\CategoriaModel::all();
    	$recetas_array=[];
    	$i=0;

    	foreach($recetas as $receta){
    		foreach($categorias as $categoria){
    			if($categoria->id==$receta->categoria_id){
    				$recetas_array[$i] = [$receta, 'categoria_slug'=>$categoria->slug];
    				$i=$i+1;
    			}
    		}
    	}
    	return view('recetas.index', compact('recetas_array'));    	
    }	

    public function ajaxGetSearchReceta(Request $request){
        $recetas_encontradas = RecetaModel::where('titulo_receta', 'LIKE', '%'.$request->word.'%')->get();
        $categorias = \App\CategoriaModel::all();
        $recetas_array=[];
        $i=0;

        foreach($recetas_encontradas as $receta){
            foreach($categorias as $categoria){
                if($categoria->id==$receta->categoria_id){
                    $recetas_array[$i] = [$receta, 'categoria_slug'=>$categoria->slug];
                    $i=$i+1;
                }
            }
        }
        return $recetas_array;
    }

}
