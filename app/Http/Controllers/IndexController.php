<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Visitor;

class IndexController extends Controller
{
    public function index(){
    	$recetas = \App\RecetaModel::all();
    	$categorias = \App\CategoriaModel::all();
    	$recetas_array=[];
    	$i=0;

    	foreach($recetas as $receta){
    		foreach($categorias as $categoria){
    			if($categoria->id==$receta->categoria_id){
    				$recetas_array[$i] = [$receta, 'categoria_slug'=>$categoria->slug];
    				$i=$i+1;
    			}
    		}
    	}

    	//return $recetas_array;
        Visitor::log();
        //$ip = Visitor::get()->ip;


    	return view('index', compact('recetas_array'));
    }
}
