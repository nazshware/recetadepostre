<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ImagenesRecetaModel;
use App\RecetaModel;

class ImagenesRecetaController extends Controller
{
    public function lista(){
    	$imagenes = ImagenesRecetaModel::all();
    	$recetas = RecetaModel::all(); 
	    return view('admin.imagenes.lista', compact('imagenes','recetas'));
	}

	public function crear(){
		$recetas = RecetaModel::all();
		return view('admin.imagenes.crear', compact('recetas'));
	}

	public function guardar(Request $request){
		$file=$request->file('imagen_receta');

		ImagenesRecetaModel::create([
			'receta_id'=>$request->receta_id,
			'titulo_imagen'=>$request->titulo_imagen,
			'descripcion_imagen'=>$request->descripcion_imagen,
			'imagen'=>$file->getClientOriginalName()
		]);

		$file->move(base_path().'/public/uploads/imagenes/recetas/',$file->getClientOriginalName());
	}

	public function mostrarImagen($id){
		$imagen = ImagenesRecetaModel::find($id);
		return view('admin.imagenes.mostrar-imagen', compact('imagen'));
	}	
}
