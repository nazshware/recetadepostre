<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\ImagenesRecetaModel;
use App\RecetaModel;

class ImagenesController extends Controller
{
    public function mostrarImagenPrincipal($slug){
    	$imagen = RecetaModel::where('imagen','=',$slug)->first();
    	return view('imagenes.mostrar-imagen', compact('imagen'));
    }
}
