<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    public function crear(){
    	return view('admin.categorias.crear');
    }	

    public function guardar(Request $request){
    	\App\CategoriaModel::create([
    		'titulo_categoria'=>$request['titulo'],
            'slug'=>str_slug($request['titulo'],'-'),
    		
    		]);

        return redirect()->route('lista-categorias');
    }	

    public function lista(){
        $categorias = \App\CategoriaModel::all();
        return view('admin.categorias.lista', compact('categorias'));
    }   

    public function editar($id){
        $categoria = \App\CategoriaModel::find($id);
        return view('admin.categorias.editar', compact('categoria'));
    } 

    public function editar_guardar(Request $request,$id){
        $categoria = \App\CategoriaModel::find($id);
        $categoria->titulo_categoria = $request->titulo;
        $categoria->slug = str_slug($request->titulo,'-');
        
        $categoria->save();

        return redirect()->route('lista-categorias');
    } 
}
