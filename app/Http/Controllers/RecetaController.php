<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\ImagenesRecetaModel;

class RecetaController extends Controller
{
    public function crear(){
        $categorias = \App\CategoriaModel::all();
        $imagenes=ImagenesRecetaModel::all();
    	return view('admin.recetas.crear', compact('categorias','imagenes'));
    }	

    public function guardar(Request $request){
        $file=$request->file('imagen');   

        if (Auth::check())
        {
            $id_user = Auth::user()->id;
        } 

    	\App\RecetaModel::create([
            'user_id'=> $id_user,    
            'categoria_id'=>$request['categoria'],
    		'titulo_receta'=>$request['titulo'],
            'slug'=>str_slug($request['titulo'],'-'),
            'contenido_receta_intro'=>$request['contenido_intro'],
    		'contenido_receta'=>$request['contenido'],
            'contenido_receta_final'=>$request['contenido_final'],
    		'breve_descripcion'=>$request['descripcion'],
            'imagen'=>$file->getClientOriginalName(),
            'imagen_descripcion'=>$request['descripcion_imagen']
    		]);

        $file->move(base_path().'/public/uploads/imagenes/recetas/',$file->getClientOriginalName());

        return redirect()->route('lista-recetas');
    }	

    public function lista(){
        $recetas = \App\RecetaModel::all();        
        return view('admin.recetas.lista', compact('recetas'));
    }   

    public function editar($id){
        $receta = \App\RecetaModel::find($id);
        $categorias = \App\CategoriaModel::all();
        return view('admin.recetas.editar', compact('receta','categorias'));
    } 

    public function editar_guardar(Request $request,$id){
        $receta = \App\RecetaModel::find($id);
        $receta->categoria_id = $request->categoria;
        $receta->titulo_receta = $request->titulo;
        $receta->slug = str_slug($request->titulo,'-');
        $receta->breve_descripcion = $request->descripcion;
        $receta->imagen_descripcion = $request->descripcion_imagen;
        $receta->contenido_receta = $request->contenido;
        $receta->contenido_receta_intro = $request->contenido_intro;
        $receta->contenido_receta_final = $request->contenido_final;
        $receta->save();

        return redirect()->route('lista-recetas');
    } 
}
