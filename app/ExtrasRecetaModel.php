<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtrasRecetaModel extends Model
{
    protected $table = 'extras';

    protected $fillable = [
    	'receta_id',
    	'porciones',
    	'tiempo_preparacion',
    ];
}
