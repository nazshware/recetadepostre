<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImagenesRecetaModel extends Model
{
    protected $table = 'imagenes';

    protected $fillable = [
    	'receta_id',
    	'titulo_imagen',
    	'descripcion_imagen',
    	'imagen'
    ];
}
