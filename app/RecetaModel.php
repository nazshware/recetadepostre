<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecetaModel extends Model
{
    protected $table = 'recetas';

    protected $fillable = [
    	'user_id',
    	'categoria_id',
    	'titulo_receta',
    	'slug',
    	'contenido_receta_intro',
        'contenido_receta',
        'contenido_receta_final',
    	'breve_descripcion',
    	'imagen',
    	'imagen_descripcion'
    ];
}
