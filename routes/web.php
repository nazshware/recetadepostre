<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

/* Rutas Publicas */
Route::get('/{categoria}/{slug}', 'RecetaPublicController@mostrarReceta')->name('mostrar-receta');
Route::get('/imagenes/principales/{slug}','ImagenesController@mostrarImagenPrincipal')->name('imagen-principal');
Route::get('/recetas', 'RecetaPublicController@getAllRecetas')->name('todas-recetas');

/* Rutas ajax */
Route::get('/ajax/get/search/receta','RecetaPublicController@ajaxGetSearchReceta');

/* Rutas Admin */
Route::prefix('admin')->group(function(){
	Route::get('/', 'AdminController@index')->name('admin');	

	Route::prefix('receta')->group(function(){
		Route::get('/crear','RecetaController@crear')->name('crear-receta');
		Route::post('/guardar','RecetaController@guardar')->name('guardar-receta');
		Route::get('/editar/{id}', 'RecetaController@editar')->name('editar-receta');
		Route::put('/editar/guardar/{id}', 'RecetaController@editar_guardar');		
	});

	Route::prefix('recetas')->group(function(){
		Route::get('/lista','RecetaController@lista')->name('lista-recetas');
	});

	Route::prefix('categoria')->group(function(){
		Route::get('/crear','CategoriaController@crear')->name('crear-categoria');
		Route::post('/guardar','CategoriaController@guardar')->name('guardar-categoria');
		Route::get('/editar/{id}', 'CategoriaController@editar')->name('editar-categoria');
		Route::put('/editar/guardar/{id}', 'CategoriaController@editar_guardar');		
	});

	Route::prefix('categorias')->group(function(){
		Route::get('/lista','CategoriaController@lista')->name('lista-categorias');			
	});

	Route::prefix('imagen')->group(function(){
		Route::get('/crear','ImagenesRecetaController@crear')->name('crear-imagen');
		Route::post('/guardar','ImagenesRecetaController@guardar')->name('guardar-imagen');
		// Route::get('/editar/{id}', 'CategoriaController@editar')->name('editar-categoria');
		// Route::put('/editar/guardar/{id}', 'CategoriaController@editar_guardar');		
	});

	Route::prefix('imagenes')->group(function(){
		Route::get('/lista','ImagenesRecetaController@lista')->name('lista-imagenes');
	});

	Route::prefix('visitas')->group(function(){
		Route::get('/principal','VisitasController@principal')->name('visitas-principal');
		
	});

	Route::prefix('uploads')->group(function(){
		Route::get('/imagenes/{id}','ImagenesRecetaController@mostrarImagen')->name('mostrar-imagen');
	});
	

});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
