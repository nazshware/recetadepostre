<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRecetas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recetas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();   
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('categoria_id')->unsigned();   
            $table->foreign('categoria_id')->references('id')->on('categorias');
            $table->string('titulo_receta',250);            
            $table->string('slug',400);
            $table->text('contenido_receta_intro');
            $table->text('contenido_receta');
            $table->text('contenido_receta_final');
            $table->text('breve_descripcion');
            $table->string('imagen',150);
            $table->string('imagen_descripcion',250);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

